#transforming from the decimal system
# into binary, octal, decimal, hexadecimal
def transform(n,base):
  if base == 10:
    return n
  m = n
  out = ""
  letter = ""
  while m != 0:
    if base != 16:
      out = str(int(m%base)) + out
    else:
      if m%base < 10:
        out = str(int(m%base)) + out
      else:
        if m%base == 10:
          letter = 'A'
        if m%base == 11:
          letter = 'B'  
        if m%base == 12:
          letter = 'C'  
        if m%base == 13:
          letter = 'D'
        if m%base == 14:
          letter = 'E'  
        if m%base == 15:
          letter = 'F'   
      out = letter + out         
    m = (m-m%base)/base
  return out

n = int(input())
for i in range(n+1):
  print(str(transform(i,10))+" "+str(transform(i,8))+" "+str(transform(i,16))+" "+str(transform(i,2)))

